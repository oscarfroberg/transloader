function imgur(info, tab)
{
        var imageurl = info.srcUrl;
        chrome.tabs.create({url: "http://imgur.com/api/upload/?url=" + imageurl});
}

function aspargus(info)
{
        var imageurl = info.srcUrl;
        chrome.tabs.create({url: "http://asparg.us/api/upload/?url=" + imageurl})
}

chrome.contextMenus.create({title: "Upload to Imgur", contexts:["image"], "onclick": imgur});
chrome.contextMenus.create({title: "Upload to asparg.us", contexts:["image"], "onclick": aspargus});


